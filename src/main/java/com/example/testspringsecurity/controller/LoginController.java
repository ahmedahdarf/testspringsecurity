package com.example.testspringsecurity.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
public class LoginController {


    @RolesAllowed("USER")
    @RequestMapping("/*")
    public  String getUer(){
        return "Hello user";
    }
    @RolesAllowed("ADMIN")
    @RequestMapping("/admin")
    public  String getAdmin(){
        return  "Hello admin";
    }
}
